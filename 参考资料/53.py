# 商品库存，格式为 [商品ID, 库存数量]
inventorys = [
    [1000, 2],   # 商品ID 1000 (假设为iPhone), 库存2
    [1001, 3],   # 商品ID 1001 (假设为iPad), 库存3
    [1002, 4],   # 商品ID 1002 (假设为MacBook), 库存4
]

# 购物车，初始为空
cart = {}

# 模拟商品ID到名称的映射
def idtoname(product_id):
    products = {1000: "iPhone", 1001: "iPad", 1002: "MacBook"}
    return products.get(product_id, "未知商品")

# 模拟商品ID到价格的映射
def idtoprice(product_id):
    prices = {1000: 5000, 1001: 4000, 1002: 20000}
    return prices.get(product_id, 0)

# 减少库存
def reduce_inventory(product_id, product_number):
    """
    减少指定商品的库存
    :param product_id: 商品ID
    :param product_number: 需要减少的数量
    """
    for inventory in inventorys:
        if inventory[0] == product_id:
            if inventory[1] < product_number:  # 如果库存不足，提示用户
                print(f"商品 {idtoname(product_id)} 库存不足")
                return False
            inventory[1] -= product_number  # 减少库存
            return True
    return False

# 增加库存
def add_inventory(product_id, product_number):
    """
    增加指定商品的库存
    :param product_id: 商品ID
    :param product_number: 增加的数量
    """
    for inventory in inventorys:
        if inventory[0] == product_id:
            inventory[1] += product_number  # 增加库存
            break

# 获取库存数量
def get_inventory(product_id):
    """
    获取指定商品的库存数量
    :param product_id: 商品ID
    :return: 库存数量
    """
    for inventory in inventorys:
        if inventory[0] == product_id:
            return inventory[1]
    return 0  # 如果商品不存在，返回0

# 添加商品到购物车
def addProduct(product_id, product_number):
    """
    将商品添加到购物车，并减少库存
    :param product_id: 商品ID
    :param product_number: 添加到购物车的数量
    """
    if get_inventory(product_id) == 0:
        print(f"商品 {idtoname(product_id)} 库存为0，不允许增加该商品")
        return

    if reduce_inventory(product_id, product_number):  # 如果库存减少成功
        if product_id in cart:
            cart[product_id] += product_number  # 如果商品已在购物车中，增加数量
        else:
            cart[product_id] = product_number  # 否则将商品加入购物车

# 从购物车删除商品
def delProduct(product_id):
    """
    从购物车中删除一个商品，并恢复库存
    :param product_id: 商品ID
    """
    if product_id in cart:
        cart[product_id] -= 1  # 购物车中该商品数量减少1
        if cart[product_id] == 0:
            cart.pop(product_id)  # 如果购物车中该商品数量为0，移除该商品
        add_inventory(product_id, 1)  # 恢复库存

# 展示购物车内容和总费用
def showCart():
    """
    显示购物车内的商品信息及总费用，并显示库存情况
    """
    totalMoney = 0
    print("购物车内容:")
    for product_id in cart:
        product_name = idtoname(product_id)
        product_quantity = cart[product_id]
        product_price = idtoprice(product_id) * product_quantity
        totalMoney += product_price  # 计算总费用
        print(f"商品 {product_name} 的数量为 {product_quantity}，费用为 {product_price}")

    print(f"总费用为: {totalMoney}\n")
    
    print("库存情况:")
    for inventory in inventorys:
        print(f"商品 {idtoname(inventory[0])} 的库存为 {inventory[1]}")

# 清空购物车
def cleanCart():
    """
    清空购物车
    """
    cart.clear()


# 示例操作
# 增加商品后
addProduct(1002, 2)  # 尝试添加2个MacBook到购物车
addProduct(1000, 5)  # 尝试添加5个iPhone到购物车
showCart()           # 显示购物车内容和总费用

# 执行结果：
# 商品 MacBook 库存不足
# 商品 iPhone 库存不足
# 购物车内容:
# 总费用为: 0
#
# 库存情况:
# 商品 iPhone 的库存为 2
# 商品 iPad 的库存为 3
# 商品 MacBook 的库存为 4


